﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public float health;

    // Projectile Firing
    public GameObject bullet;
    public GameObject turret;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        FireBullet();
    }

    private void OnCollisionEnter(Collision other) 
    {
        
        if(other.gameObject.tag == "projectile")
        {
            if(this.health > 0)
            {
                TakeDamage();
            }
            else if (this.health <= 0)
            {
                Destroy(this.gameObject);
                Debug.Log("Player Tank Destroyed!");
            }
        }
        
    }

    void TakeDamage()
    {
        
        this.health -= 10;
        Debug.Log("Player taking damage by 10");
    }

    void FireBullet()
    {
        if (Input.GetMouseButtonDown(0))
        {
            GameObject b = Instantiate(bullet, turret.transform.position, turret.transform.rotation);
            b.GetComponent<Rigidbody>().AddForce(turret.transform.forward *700);
        }
    }
}
