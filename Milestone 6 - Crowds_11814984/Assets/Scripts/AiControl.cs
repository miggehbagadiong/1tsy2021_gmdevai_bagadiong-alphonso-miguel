﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AiControl : MonoBehaviour
{
    GameObject[] goalLocations;
    NavMeshAgent agent;
    Animator animator;
    float speedMultiplier;
    //for fleeing
    float detectionRadius = 20f;
    float fleeRadius = 10f;
    float flockRadius = 15f;

    void ResetAgent(){
        speedMultiplier = Random.Range(0.1f, 1.5f);
        agent.speed = 2 * speedMultiplier;
        agent.angularSpeed = 120;
        animator.SetFloat("speedMultiplier", speedMultiplier);
        animator.SetTrigger("isWalking");


    }
    // Start is called before the first frame update
    void Start()
    {
        goalLocations = GameObject.FindGameObjectsWithTag("goal");
        agent = this.GetComponent<NavMeshAgent>();
        agent.SetDestination(goalLocations[Random.Range(0,goalLocations.Length)].transform.position);
        animator = this.GetComponent<Animator>();
        animator.SetFloat("wOffset", Random.Range(0.1f, 1f));
        ResetAgent();   
    }

    // Update is called once per frame
    void Update()
    {
        if(agent.remainingDistance < 1){
            ResetAgent();
            agent.SetDestination(goalLocations[Random.Range(0, goalLocations.Length)].transform.position);
        }
    }

    // detecting obstacle that makes the agents flee (*lecture)
    public void DetectNewObstacle(Vector3 location){

        if (Vector3.Distance(location, this.transform.position) < detectionRadius){
            Vector3 fleeDirection = (this.transform.position + location).normalized;
            Vector3 newGoal = this.transform.position + fleeDirection * fleeRadius;

            NavMeshPath path = new NavMeshPath();
            agent.CalculatePath(newGoal, path);

            if (path.status != NavMeshPathStatus.PathInvalid){
                agent.SetDestination(path.corners[path.corners.Length - 1]);
                animator.SetTrigger("isRunning");
                agent.speed = 10;
                agent.angularSpeed = 500;
            }
        }
    }

    // detecting objects that would make other agents flock
    public void DetectAndFlockNewObstacle(Vector3 location){

        if(Vector3.Distance(location, this.transform.position) < detectionRadius){
            Vector3 flockDirection = (this.transform.position - location).normalized;
            Vector3 flockGoal = this.transform.position - (flockDirection * flockRadius);

            NavMeshPath flockPath = new NavMeshPath();
            agent.CalculatePath(flockGoal, flockPath);

            if(flockPath.status != NavMeshPathStatus.PathInvalid){
                agent.SetDestination(flockPath.corners[flockPath.corners.Length - 1]);
                animator.SetTrigger("isRunning");
                agent.speed = 10;
                agent.angularSpeed = 500;
            }
        }
    }
}
