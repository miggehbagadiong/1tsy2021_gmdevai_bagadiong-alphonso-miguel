﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{

    public float speed = 5;

    void Start()
    {
        
    }

    void Update()
    {
      float xTranslate = Input.GetAxis("Horizontal") * speed * Time.deltaTime;
      float zTranslate = Input.GetAxis("Vertical") * speed * Time.deltaTime;
      this.transform.position += new Vector3(xTranslate, 0, zTranslate);
    }
}

/*
References:

*/