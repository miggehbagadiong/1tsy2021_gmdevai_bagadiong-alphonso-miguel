﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PetFollow : MonoBehaviour
{
    public Transform owner;

    //pre-initialize public variables
    public float speed = 10;
    public float rotSpeed = 5;

    void Start()
    {
        
    }

    
    void LateUpdate()
    {
        Vector3 lookAtOwner = new Vector3(owner.position.x,
                                        this.transform.position.y,
                                        owner.position.z);

        transform.LookAt(lookAtOwner);

        Vector3 direction = lookAtOwner - transform.position;

        this.transform.rotation = Quaternion.Slerp(this.transform.rotation,
                                                            Quaternion.LookRotation(direction),
                                                            Time.deltaTime * rotSpeed);

        if(Vector3.Distance(lookAtOwner, transform.position) > 8){
            transform.Translate(0,0, speed * Time.deltaTime);
        }
    }
}