﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AgentManager : MonoBehaviour
{
    GameObject[] agents;
    public Transform player;

    // Start is called before the first frame update
    void Start()
    {
        agents = GameObject.FindGameObjectsWithTag("AI");       
    }

    // Update is called once per frame
    void Update()
    {
        foreach(GameObject ai in agents){
            ai.GetComponent<AiController>().agent.SetDestination(player.position);
        }
    }
}
